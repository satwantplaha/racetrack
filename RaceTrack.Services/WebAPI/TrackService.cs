﻿using System.Threading.Tasks;
using System;
using RaceTrack.Models.Common;
using RaceTrack.Services.Shared;
using RaceTrack.Data.EF;
using System.Linq;
using System.Data.Entity;
using RaceTrack.Models.DTOs.WebAPI;
using static RaceTrack.Models.Enums.Enum;
using RaceTrack.Models.DTOs;

namespace RaceTrack.Services.WebAPI
{
    public interface ITrackService : IDisposable
    {
        Task<IResponse> GetCurrentTrack();

        Task<int> GetCurrentTrackCount();
    }

    public class TrackService : ITrackService
    {
        private readonly IResponseService responseService;

        public TrackService(
            IResponseService responseService
            )
        {
            this.responseService = responseService;
        }

        public async Task<IResponse> GetCurrentTrack()
        {
            using (var db = new EntityContext())
            {
                var vehicles = (from trk in db.Track
                                join veh in db.Vehicle on trk.VehicleId equals veh.VehicleId into vins
                                from v in vins.DefaultIfEmpty()
                                select new { vehicle = v});

                var list = await vehicles.Select(s => new VehicleDTO
                {
                    VehicleId = s.vehicle.VehicleId,
                    Title = s.vehicle.Title,
                    Type = ((VehicleType)s.vehicle.VehicleType).ToString()
                }).ToListAsync<IDTO>();

                return responseService.Success("List", list);
            }
        }

        public async Task<int> GetCurrentTrackCount()
        {
            using (var db = new EntityContext())
            {
                var vcls = from trk in db.Track
                           select 1;

                return await vcls.CountAsync();
            }
        }

        public void Dispose()
        {

        }
    }
}
