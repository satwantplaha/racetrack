﻿using System.Threading.Tasks;
using System.Linq;
using System;
using RaceTrack.Models.Common;
using RaceTrack.Data.EF;
using RaceTrack.Services.Shared;
using System.Data.Entity;
using RaceTrack.Models.DTOs;
using RaceTrack.Data.DataModels.Dbo;
using static RaceTrack.Models.Enums.Enum;

namespace RaceTrack.Services.WebAPI
{
    public interface IInspectionService : IDisposable
    {
        Task SetupInspections();

        Task<IResponse> GetInspections(int vehicleType);

        Task<int> GetInspectionsCount(int vehicleType);


    }

    public class InspectionService : IInspectionService
    {
        private readonly IResponseService responseService;

        public InspectionService(
            IResponseService responseService
            )
        {
            this.responseService = responseService;
        }

        public async Task SetupInspections()
        {
            using (var db = new EntityContext())
            {
                var inspections = from inspection in db.Inspection
                                  select 1;

                var count = await inspections.CountAsync();

                if(count == 0)
                {
                    db.Inspection.Add(new Inspection { InspectionId = Guid.NewGuid(), VehicleType = (byte)VehicleType.Car, Title = "Tow strap on the vehicle" });
                    db.Inspection.Add(new Inspection { InspectionId = Guid.NewGuid(), VehicleType = (byte)VehicleType.Car, Title = "Less than 85% tire wear" });
                    db.Inspection.Add(new Inspection { InspectionId = Guid.NewGuid(), VehicleType = (byte)VehicleType.Truck, Title = "Tow strap on the vehicle" });
                    db.Inspection.Add(new Inspection { InspectionId = Guid.NewGuid(), VehicleType = (byte)VehicleType.Truck, Title = "Not lifted more than 5 inches" });

                    _ = await db.SaveChangesAsync();
                }
            }
        }

        public async Task<IResponse> GetInspections(int vehicleType)
        {
            using (var db = new EntityContext())
            {
                var inspections = from inspection in db.Inspection
                           where inspection.VehicleType == vehicleType
                           select inspection;

                var list = await inspections.Select(s => new SelectListItem
                {
                    Value = s.InspectionId,
                    Text = s.Title,
                    IsSelected = false
                }).ToListAsync<IDTO>();

                return responseService.Success("List", list);
            }
        }

        public async Task<int> GetInspectionsCount(int vehicleType)
        {
            using (var db = new EntityContext())
            {
                var inspections = from inspection in db.Inspection
                                  where inspection.VehicleType == vehicleType
                                  select 1;

                return await inspections.CountAsync();
            }
        }

        public void Dispose()
        {
            
        }
    }
}
