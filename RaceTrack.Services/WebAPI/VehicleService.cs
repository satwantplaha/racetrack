﻿using System.Threading.Tasks;
using System.Linq;
using System;
using RaceTrack.Models.Common;
using RaceTrack.Data.EF;
using RaceTrack.Services.Shared;
using RaceTrack.Models.DTOs;
using System.Data.Entity;
using RaceTrack.Data.DataModels.Dbo;
using RaceTrack.Models.ViewModels.WebAPI;
using RaceTrack.Models.DTOs.WebAPI;
using static RaceTrack.Models.Enums.Enum;

namespace RaceTrack.Services.WebAPI
{
    public interface IVehicleService : IDisposable
    {
        Task<IResponse> GetVehicles();

        Task<IResponse> CreateVehicle(VehicleViewModel viewModel);

        Task<IResponse> ReInspectVehicle(VehicleViewModel viewModel);

        Task<IResponse> SendVehicleToTrack(Guid vehicleId, VehicleType vehicleType);

        Task<IResponse> RemoveVehicleFromTrack(Guid vehicleId);
    }

    public class VehicleService : IVehicleService
    {
        private readonly IResponseService responseService;
        private readonly IInspectionService inspectionService;
        private readonly ITrackService trackService;

        public VehicleService(
            IResponseService responseService,
            IInspectionService inspectionService,
            ITrackService trackService
            )
        {
            this.responseService = responseService;
            this.inspectionService = inspectionService;
            this.trackService = trackService;
        }


        public async Task<IResponse> GetVehicles()
        {
            var carInsCount = await this.inspectionService.GetInspectionsCount((byte)VehicleType.Car);
            var truckInsCount = await this.inspectionService.GetInspectionsCount((byte)VehicleType.Truck);

            using (var db = new EntityContext())
            {
                var vehicles = (from vehicle in db.Vehicle
                                join ins in db.VehicleInspection on vehicle.VehicleId equals ins.VehicleId into vins
                                join trck in db.Track on vehicle.VehicleId equals trck.VehicleId into track
                                from t in track.DefaultIfEmpty()
                                select new { vehicle, insCount = vins.Count(), onTrack = t });

                var list = await vehicles.Select(s => new VehicleDTO
                {
                    VehicleId = s.vehicle.VehicleId,
                    Title = s.vehicle.Title,
                    Type = ((VehicleType)s.vehicle.VehicleType).ToString(),
                    DoPassedInspection = s.insCount == (s.vehicle.VehicleType == (byte)VehicleType.Car ? carInsCount : truckInsCount),
                    IsOnTrack = s.onTrack != null
                }).ToListAsync<IDTO>();

                return responseService.Success("List", list);
            }
        }

        public async Task<IResponse> CreateVehicle(VehicleViewModel viewModel)
        {
            using (var db = new EntityContext())
            {

                viewModel.VehicleId = Guid.NewGuid();

                var vehicle = new Vehicle
                {
                    VehicleId = viewModel.VehicleId,
                    Title = viewModel.Title,
                    VehicleType= viewModel.VehicleType
                };


                db.Vehicle.Add(vehicle);

                foreach (var ins in viewModel.Inspections)
                {
                    if(ins.IsSelected == true)
                    {
                        db.VehicleInspection.Add(new VehicleInspection { VehicleInspectionId = Guid.NewGuid(), VehicleId = vehicle.VehicleId, InspectionId = ins.Value });
                    }
                }                

                _ = await db.SaveChangesAsync();

                return responseService.Success("Create", viewModel);
            }
        }

        public async Task<IResponse> SendVehicleToTrack(Guid vehicleId, VehicleType vehicleType)
        {
            var trackCount = await this.trackService.GetCurrentTrackCount();

            if(trackCount == 5)
            {
                return responseService.Error("Track is full !");
            }

            var insCount = await this.inspectionService.GetInspectionsCount((byte)vehicleType);

            using (var db = new EntityContext())
            {
                var vehicle = await (from veh in db.Vehicle
                                join ins in db.VehicleInspection on veh.VehicleId equals ins.VehicleId into vins
                                join trck in db.Track on veh.VehicleId equals trck.VehicleId into track
                                from t in track.DefaultIfEmpty()
                                where veh.VehicleId == vehicleId
                                select new { vehicle= veh, insCount = vins.Count(), onTrack = t }).FirstOrDefaultAsync();

                if (vehicle.onTrack != null)
                {
                    return responseService.Error("Vehicle Already On Track");
                }

                if(vehicle.insCount != insCount)
                {
                    return responseService.Error("Inspection Failed");
                }


                db.Track.Add(new Track { TrackId = Guid.NewGuid(), VehicleId = vehicleId });

                _ = await db.SaveChangesAsync();

                return responseService.Success("Sent to track");
            }
        }

        public async Task<IResponse> ReInspectVehicle(VehicleViewModel viewModel)
        {
            using (var db = new EntityContext())
            {
                var vehicle = await (from veh in db.Vehicle
                                     where veh.VehicleId == viewModel.VehicleId
                                     select veh).FirstOrDefaultAsync();

                // to clear previous inspections
                var previous = await (from ins in db.VehicleInspection
                                      where ins.VehicleId == viewModel.VehicleId
                                      select ins).ToListAsync();

                foreach (var ins in previous)
                {
                    db.VehicleInspection.Remove(ins);
                }

                // to clear previous inspections end

                foreach (var ins in viewModel.Inspections)
                {
                    if (ins.IsSelected == true)
                    {
                        db.VehicleInspection.Add(new VehicleInspection { VehicleInspectionId = Guid.NewGuid(), VehicleId = vehicle.VehicleId, InspectionId = ins.Value });
                    }
                }

                _ = await db.SaveChangesAsync();

                return responseService.Success("Re-Inspect", viewModel);
            }
        }

        public async Task<IResponse> RemoveVehicleFromTrack(Guid vehicleId)
        {
            using (var db = new EntityContext())
            {
                var vehicle = await (from veh in db.Track                                     
                                     where veh.VehicleId == vehicleId
                                     select veh).FirstOrDefaultAsync();                

                db.Track.Remove(vehicle);

                _ = await db.SaveChangesAsync();

                return responseService.Success("removed from track");
            }
        }


        public void Dispose()
        {
            
        }
    }
}
