﻿using RaceTrack.Models.Common;
using RaceTrack.Models.DTOs;
using RaceTrack.Models.ViewModels.Interface;
using System.Collections.Generic;

namespace RaceTrack.Services.Shared
{
    public interface IResponseService
    {
        IResponse Success(string message);

        IResponse Success(string message, List<IDTO> items);

        IResponse Success(string message, IViewModel viewModel);

        IResponse Error(string message);
    }

    public class ResponseService : IResponseService
    {
        public IResponse Success(string message)
        {
            return new Response
            {
                 Status = ResponseStatus.Success,
                 Message = message
            };
        }

        public IResponse Success(string message, List<IDTO> items)
        {
            return new Response
            {
                Status = ResponseStatus.Success,
                Message = message,
                Items = items
            };
        }

        public IResponse Success(string message, IViewModel viewModel)
        {
            return new Response
            {
                Status = ResponseStatus.Success,
                Message = message,
                Model = viewModel
            };
        }

        public IResponse Warning(string message)
        {
            return new Response
            {
                Status = ResponseStatus.Warning,
                Message = message
            };
        }

        public IResponse Error(string message)
        {
            return new Response
            {
                Status = ResponseStatus.Error,
                Message = message
            };
        }

        public IResponse Information(string message)
        {
            return new Response
            {
                Status = ResponseStatus.Info,
                Message = message
            };
        }
    }
}
