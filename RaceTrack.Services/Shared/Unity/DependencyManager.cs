﻿using System.Collections.Generic;
using RaceTrack.Models.Common;
using RaceTrack.Services.WebAPI;

namespace RaceTrack.Services.Shared.Unity
{
    public static class DependencyManager
    {
        public static List<Dependency> GetDependencies()
        {
            return new List<Dependency>
            {
                new Dependency{ From = typeof(IResponseService), To=typeof(ResponseService) },

                new Dependency{ From = typeof(IVehicleService), To=typeof(VehicleService) },

                new Dependency{ From = typeof(IInspectionService), To=typeof(InspectionService) },

                new Dependency{ From = typeof(ITrackService), To=typeof(TrackService) },
            };
        }
    }
}
