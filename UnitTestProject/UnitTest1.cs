﻿using System;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RaceTrack.Models.Common;
using RaceTrack.Models.ViewModels.WebAPI;
using RaceTrack.Services.Shared;
using RaceTrack.Services.WebAPI;
using static RaceTrack.Models.Enums.Enum;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public async Task TrySendNewVehicleToTrack()
        {            
            IResponseService responseService = new ResponseService();
            ITrackService trackService = new TrackService(responseService);
            IInspectionService inspectionService = new InspectionService(responseService);
            IVehicleService vehicleService = new VehicleService(responseService,inspectionService,trackService);



            Guid vehicleId = new Guid("c296568e-422e-472e-a63e-658554bfa7c1");

            var vehicleType = VehicleType.Car;

            var response = (Response)await vehicleService.SendVehicleToTrack(vehicleId, vehicleType);


            var trackCount = await trackService.GetCurrentTrackCount();

            if(trackCount > 5)
            {
                Assert.Fail("Fail : entering more than 5 vehicles to track.");
            }
        }
    }
}
