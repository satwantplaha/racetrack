﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;

namespace RaceTrack.Data.DataModels.Dbo
{
    public class InspectionConfiguration : EntityTypeConfiguration<Inspection>
    {
        public InspectionConfiguration()
        {
            ToTable("Inspection", schemaName: "dbo");

            HasKey(t => t.InspectionId);

            Property(x => x.InspectionId).IsRequired();

            Property(x => x.Title).IsRequired().HasColumnType("varchar").HasMaxLength(250);

            Property(x => x.VehicleType).HasColumnType("int").IsRequired();
        }
    }

    public class Inspection
    {
        public Guid InspectionId { get; set; }

        public int VehicleType { get; set; }

        public string Title { get; set; }
    }

}
