﻿using RaceTrack.Data.DataModels.Dbo;
using System;
using System.Data.Entity.ModelConfiguration;

namespace RaceTrack.Data.DataModels.Dbo
{
    public class TrackConfiguration : EntityTypeConfiguration<Track>
    {
        public TrackConfiguration()
        {
            ToTable("Track", schemaName: "dbo");

            HasKey(t => t.TrackId);

            Property(x => x.TrackId).IsRequired();
        }
    }

    public class Track
    {
        public Guid TrackId { get; set; }

        public Guid VehicleId { get; set; }
    }
}
