﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;

namespace RaceTrack.Data.DataModels.Dbo
{
    public class VehicleInspectionConfiguration : EntityTypeConfiguration<VehicleInspection>
    {
        public VehicleInspectionConfiguration()
        {
            ToTable("VehicleInspection", schemaName: "dbo");

            HasKey(t => t.VehicleInspectionId);

            Property(x => x.VehicleInspectionId).IsRequired();
        }
    }

    public class VehicleInspection
    {
        public Guid VehicleInspectionId { get; set; }

        public Guid VehicleId { get; set; }

        public Guid InspectionId { get; set; }
    }


}
