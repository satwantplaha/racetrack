﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;

namespace RaceTrack.Data.DataModels.Dbo
{
    public class VehicleConfiguration : EntityTypeConfiguration<Vehicle>
    {
        public VehicleConfiguration()
        {
            ToTable("Vehicle", schemaName: "dbo");

            HasKey(t => t.VehicleId);

            Property(x => x.VehicleId).IsRequired();

            Property(x => x.Title).IsRequired().HasColumnType("varchar").HasMaxLength(250);

            Property(x => x.VehicleType).HasColumnType("int").IsRequired();

        }
    }

    public class Vehicle
    {
        public Guid VehicleId { get; set; }
       
        public string Title { get; set; }

        public int VehicleType { get; set; }
    }

}
