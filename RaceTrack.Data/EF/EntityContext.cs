using System.Data.Entity;
using RaceTrack.Data.DataModels.Dbo;

namespace RaceTrack.Data.EF
{
    public class EntityContext : DbContext
    {
        public EntityContext() : base("RaceTrackDB")
        {
            //Database.SetInitializer<EntityContext>(new DropCreateDatabaseAlways<EntityContext>());

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new VehicleConfiguration());

            modelBuilder.Configurations.Add(new InspectionConfiguration());

            modelBuilder.Configurations.Add(new VehicleInspectionConfiguration());

            modelBuilder.Configurations.Add(new TrackConfiguration());

            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Vehicle> Vehicle { get; set; }

        public DbSet<Inspection> Inspection { get; set; }

        public DbSet<VehicleInspection> VehicleInspection { get; set; }

        public DbSet<Track> Track { get; set; }
    }
}