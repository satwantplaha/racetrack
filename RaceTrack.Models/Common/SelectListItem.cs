﻿using RaceTrack.Models.DTOs;
using System;

namespace RaceTrack.Models.Common
{
    public class SelectListItem : IDTO
    {
        public string Text { get; set; }

        public Guid Value { get; set; }

        public bool IsSelected { get; set; }
    }
}
