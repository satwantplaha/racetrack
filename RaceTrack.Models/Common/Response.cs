﻿using RaceTrack.Models.DTOs;
using RaceTrack.Models.ViewModels.Interface;
using System.Collections.Generic;

namespace RaceTrack.Models.Common
{
    public interface IResponse
    {

    }

    public class Response : IResponse
    {
        public ResponseStatus Status { get; set; }

        public string Message { get; set; }

        public IViewModel Model { get; set; }

        public List<IDTO> Items { get; set; }
    }

    public enum ResponseStatus
    {
        Info = 0,
        Warning = 1,
        Success = 2,
        Error = 3
    }
}
