﻿using System;

namespace RaceTrack.Models.Common
{
    public class Dependency
    {
        public Type From { get; set; }

        public Type To { get; set; }

        public bool SingletonLifeTime { get; set; } = false;

        public bool InjectParameter { get; set; } = false;

        public object[] ParameterValues { get; set; }
    }
}
