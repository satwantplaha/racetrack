﻿using RaceTrack.Models.Common;
using RaceTrack.Models.ViewModels.Interface;
using System;
using System.Collections.Generic;

namespace RaceTrack.Models.ViewModels.WebAPI
{
    public class VehicleViewModel : IViewModel 
    {
        public Guid VehicleId { get; set; }

        public string Title { get; set; }

        public int VehicleType { get; set; }

        public List<SelectListItem> Inspections { get; set; } = new List<SelectListItem>();

    }
}
