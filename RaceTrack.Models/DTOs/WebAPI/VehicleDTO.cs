﻿using System;

namespace RaceTrack.Models.DTOs.WebAPI
{
    public class VehicleDTO : IDTO
    {
        public Guid VehicleId { get; set; }

        public string Title { get; set; }

        public string Type { get; set; }

        public bool DoPassedInspection { get; set; }

        public bool IsOnTrack { get; set; }
    }
}
