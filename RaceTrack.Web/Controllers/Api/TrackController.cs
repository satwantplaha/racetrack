﻿using RaceTrack.Services.WebAPI;
using System;
using System.Threading.Tasks;
using System.Web.Http;

namespace RaceTrack.Controllers.Api.Web
{    
    [RoutePrefix("api/Web/Track")]    
    public class TrackController : ApiController
    {
        private readonly ITrackService trackService;
        public TrackController(
         ITrackService trackService
            )
        {
           this.trackService = trackService;
        }

        [HttpGet]
        [Route("GetCurrentTrack")]
        public async Task<IHttpActionResult> GetCurrentTrack()
        {
            var response = await this.trackService.GetCurrentTrack();

            return Ok(response);
        }


        protected override void Dispose(bool disposing)
        {
            this.trackService.Dispose();
            base.Dispose(disposing);
        }
    }
}
