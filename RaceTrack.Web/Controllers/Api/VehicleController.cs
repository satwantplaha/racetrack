﻿using RaceTrack.Models.ViewModels.WebAPI;
using RaceTrack.Services.WebAPI;
using System;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using static RaceTrack.Models.Enums.Enum;

namespace RaceTrack.Controllers.Api.Web
{    
    [RoutePrefix("api/Web/Vehicle")]    
    public class VehicleController : ApiController
    {
        private readonly IVehicleService vehicleService;
        public VehicleController(
         IVehicleService vehicleService
            )
        {
           this.vehicleService = vehicleService;
        }

        [HttpGet]
        [Route("GetVehicles")]
        public async Task<IHttpActionResult> GetVehicles()
        {
            var response = await this.vehicleService.GetVehicles();

            return Ok(response);
        }

        [HttpPost]
        [Route("Create")]
        public async Task<IHttpActionResult> Create(VehicleViewModel viewModel)
        {
            var response = await this.vehicleService.CreateVehicle(viewModel);

            return Ok(response);
        }

        [HttpPost]
        [Route("ReInspectVehicle")]
        public async Task<IHttpActionResult> ReInspectVehicle(VehicleViewModel viewModel)
        {
            var response = await this.vehicleService.ReInspectVehicle(viewModel);

            return Ok(response);
        }

        [HttpPost]
        [Route("SendToTrack")]
        public async Task<IHttpActionResult> SendToTrack(Guid vehicleId, VehicleType vehicleType)
        {
            var response = await this.vehicleService.SendVehicleToTrack(vehicleId, vehicleType);

            return Ok(response);
        }

        [HttpPost]
        [Route("RemoveVehicleFromTrack")]
        public async Task<IHttpActionResult> RemoveVehicleFromTrack(Guid vehicleId)
        {
            var response = await this.vehicleService.RemoveVehicleFromTrack(vehicleId);

            return Ok(response);
        }


        protected override void Dispose(bool disposing)
        {
            this.vehicleService.Dispose();
            base.Dispose(disposing);
        }
    }
}
