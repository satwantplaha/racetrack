﻿using RaceTrack.Services.WebAPI;
using System;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace RaceTrack.Controllers.Api.Web
{    
    [RoutePrefix("api/Web/Inspection")]    
    public class InspectionController : ApiController
    {
        private readonly IInspectionService inspectionService;
        public InspectionController(
         IInspectionService inspectionService
            )
        {
           this.inspectionService = inspectionService;
        }

        [HttpPost]
        [Route("SetupInspections")]
        public async Task<IHttpActionResult> SetupInspections()
        {
            await this.inspectionService.SetupInspections();

            return Ok("Okay");
        }

        
        [HttpGet]
        [Route("GetInspections")]
        public async Task<IHttpActionResult> GetInspections(byte vehicleType)
        {
            var response = await this.inspectionService.GetInspections(vehicleType);

            return Ok(response);
        }

        protected override void Dispose(bool disposing)
        {
            this.inspectionService.Dispose();
            base.Dispose(disposing);
        }
    }
}
