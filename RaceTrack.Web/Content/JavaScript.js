﻿
var apiPrefix = '/api/Web/';

var setUpInspections = function () {
    $.ajax({
        type: "POST",
        url: apiPrefix + "Inspection/SetupInspections",
        cache: false,
        dataType: "json",
        contentType: "application/json",
        success: function (result) {
            console.log(result);

            loadVehicles();

            let intrvl = setInterval(function () {
                loadTrack();
            }, 5000);

            $('.loading').hide();
        },
        error: function () {
            $('.toast').text('Error happened !');
            $('.toast').show();
        }
    });
};

var loadVehicles = function () {
    $(".vlist").html('');

    $.ajax({
        url: apiPrefix + "Vehicle/GetVehicles",
        cache: false,
        dataType: "json",
        contentType: "application/json",
        success: function (result) {
            var html = '';

            for (let i = 0; i < result.items.length; i++) {
                html += '<tr>';
                html += '<td>' + (result.items[i].isOnTrack ? '<i class="fas fa-flag-checkered" title="On Track"></i>' : '') + '</td>';
                html += '<td>' + result.items[i].type + '</td>';
                html += '<td>' + result.items[i].title + '</td>';
                html += '<td class="text-center">' + (result.items[i].doPassedInspection ? '<i class="fas fa-check-circle" title="Passed"></i>' : '<i class="fas fa-times-circle" title="Failed"></i>') + '</td>';
                html += '<td>' + (result.items[i].isOnTrack ? '' : result.items[i].doPassedInspection ? '<button type="button" class="btn btn-success btn-xs btn-send-to-track" data-value="' + result.items[i].vehicleId + '" data-type="' + result.items[i].type + '">Send To Track</button>' : '<button type="button" class="btn btn-info btn-xs btn-re-inspect" data-value="' + result.items[i].vehicleId + '" data-type="' + result.items[i].type + '" data-title="' + result.items[i].title + '">Re-Inspect</button>') +'</td>';
                html += '</tr>';
            }

            $(".vlist").html(html);

            setTimeout(function () { bindAllVehiclesTableButtons(); }, 100);
        },
        error: function () {
            $('.toast').text('Error happened !');
            $('.toast').show();
        }
    });
};

var loadTrack = function () {
    $(".tlist").html('');

    $.ajax({
        url: apiPrefix + "Track/GetCurrentTrack",
        cache: false,
        dataType: "json",
        contentType: "application/json",
        success: function (result) {
            var html = '';

            for (let i = 0; i < result.items.length; i++) {
                html += '<tr>';
                html += '<td>' + result.items[i].type + '</td>';
                html += '<td>' + result.items[i].title + '</td>';
                html += '<td><button type="button" class="btn btn-danger btn-xs btn-send-remove-track" data-value="' + result.items[i].vehicleId + '">Remove</button></td>';
                html += '</tr>';
            }

            $(".tlist").html(html);

            setTimeout(function () { bindTrackTableButtons(); }, 100);
        },
        error: function () {
            $('.toast').text('Error happened !');
            $('.toast').show();
        }
    });
};

var bindAllVehiclesTableButtons = function () {
    $('.btn-re-inspect').on('click', function () {
        let vehicleId = $(this).data('value');
        let vehicleType = $(this).data('type');
        let title = $(this).data('title');

        $('.re-ins-chk-box').html('');

        $.ajax({
            url: apiPrefix + "Inspection/GetInspections?vehicleType=" + (vehicleType.toLowerCase() === 'car' ? 0 : 1),
            cache: false,
            dataType: "json",
            contentType: "application/json",
            success: function (result) {
                var html = '';
                for (let i = 0; i < result.items.length; i++) {
                    html += '<label class="chklbl"><input type="checkbox" class="re-ins-chk" data-value="' + result.items[i].value + '"> ' + result.items[i].text + '</label>';
                }
                $('.re-ins-chk-box').html(html);

                $("#myModal2").modal("show");

                $('#vehicleId2').val(vehicleId);
                $('#title2').val(title);
                $('#type2').val(vehicleType);
            },
            error: function () {
                $('.toast').text('Error happened !');
                $('.toast').show();
            }
        });
    });

    $('.btn-send-to-track').on('click', function () {
        if (confirm("Sure to send to track ?")) {
            let vehicleId = $(this).data('value');
            let vehicleType = $(this).data('type');

            $.ajax({
                type: "POST",
                url: apiPrefix + "Vehicle/SendToTrack?vehicleId=" + vehicleId + "&vehicleType=" + vehicleType,
                cache: false,
                dataType: "json",
                contentType: "application/json",
                success: function (result) {
                    if (result.status === 2) {
                        loadVehicles();
                        loadTrack();
                    } else {
                        $('.toast').text(result.message);
                        $('.toast').show();
                    }
                },
                error: function () {
                    $('.toast').text('Error happened !');
                    $('.toast').show();
                }
            });
        }
    });
};

var bindTrackTableButtons = function () {
    $('.btn-send-remove-track').on('click', function () {
        if (confirm("Sure to remove from track ?")) {
            let vehicleId = $(this).data('value');

            $.ajax({
                type: "POST",
                url: apiPrefix + "Vehicle/RemoveVehicleFromTrack?vehicleId=" + vehicleId,
                cache: false,
                dataType: "json",
                contentType: "application/json",
                success: function (result) {
                    loadVehicles();
                    loadTrack();
                },
                error: function () {
                    $('.toast').text('Error happened !');
                    $('.toast').show();
                }
            });
        }
    });
};

$(document).ready(function () {

    setUpInspections();

    $('.toast').on('click', function () {
        $(this).hide();
    });

    $('#type').on('change', function (e) {
        $('.ins-chk-box').html('');
        if (e.target.value) {            
            $.ajax({
                url: apiPrefix + "Inspection/GetInspections?vehicleType=" + e.target.value,
                cache: false,
                dataType: "json",
                contentType: "application/json",
                success: function (result) {
                    var html = '';
                    for (let i = 0; i < result.items.length; i++) {
                        html += '<label class="chklbl"><input type="checkbox" class="ins-chk" data-value="' + result.items[i].value + '"> ' + result.items[i].text + '</label>';
                    }
                    $('.ins-chk-box').html(html);
                },
                error: function () {
                    $('.toast').text('Error happened !');
                    $('.toast').show();
                }
            });
        }
    });


    $('.add-new-vehicle').on('click', function (e) {
        if ($('#title').val() === '' || $('#type').val() === '') {
            $('.toast').text('Provide all fields !');
            $('.toast').show();
            return;
        }

        let ins = [];

        $('.ins-chk').each(function () {
            ins.push({
                Value: $(this).data('value'),
                IsSelected: $(this).is(':checked')
            });
        });

        $.ajax({
            type: "POST",
            url: apiPrefix + "Vehicle/Create",
            data: JSON.stringify({
                Title: $('#title').val(),
                VehicleType: $('#type').val(),
                Inspections: ins
            }),
            cache: false,
            dataType: "json",
            contentType: "application/json",
            success: function (result) {
                $("#myModal").modal("hide");
                loadVehicles();

            },
            error: function () {
                $('.toast').text('Error happened !');
                $('.toast').show();
            }
        });
    });

    $('.re-inspect-vehicle').on('click', function (e) {      

        let ins = [];

        $('.re-ins-chk').each(function () {
            ins.push({
                Value: $(this).data('value'),
                IsSelected: $(this).is(':checked')
            });
        });

        $.ajax({
            type: "POST",
            url: apiPrefix + "Vehicle/ReInspectVehicle",
            data: JSON.stringify({
                VehicleId: $('#vehicleId2').val(),
                Inspections: ins
            }),
            cache: false,
            dataType: "json",
            contentType: "application/json",
            success: function (result) {
                $("#myModal2").modal("hide");
                loadVehicles();

            },
            error: function () {
                $('.toast').text('Error happened !');
                $('.toast').show();
            }
        });
    });

    $("#myModal").on('hidden.bs.modal', function () {
        $('#title').val('');
        $('#type').val('');
        $('.ins-chk-box').html('');
    });

    $("#myModal2").on('hidden.bs.modal', function () {
        $('#title2').val('');
        $('#type2').val('');
        $('.re-ins-chk-box').html('');
    });
});