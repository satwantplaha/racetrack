Notes:

Please run RaceTrack.sln, then right click RaceTrack.Web to view in browser.
If error in view try build before view.

To check inspection status, currently comparing number of total inspections with how many checked or selected.

Track table refreshes every 5 sec. also on saving changes in both tables.

For re-inspect start checking inspections from beginning.

Prefer Composition over inheritance where classes are more complex and need changes often during development cycle. Using inheritance for simple models which are more concrete like Response, DTO, Viewmodel.

my other email is satwant_2283@hotmail.com


Thanks!